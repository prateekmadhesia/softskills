# [SQL](https://en.wikipedia.org/wiki/SQL)

***

**SQL** stands for "_Structured Query Language_", it is used to store the data in the database in the form of table and perform some operation like insertion of data, updating the data, deletion of data and modifying database tables, views, etc.

## Why SQL

***

**SQL** helps us to describe data in the structured format. We can easily manipulate data in the relational database, and also reduce the data redundancy and inconsistency.

## Types of SQL Statements

***

1. **DDL**
2. **DML**
3. **DCL**
4. **TCL**

![Type of SQL](https://media.geeksforgeeks.org/wp-content/uploads/sql-commands.jpg)

## DDL (Data Definition Language)

***
Data Definition Language(DDL) is used to define or create the database structure or schema. Common example of DDL statements are `CREATE`, `ALTER`, `DROP`, etc.

## Some Examples

### CREATE

This command is used to create the database and tables.

```SQL
  CREATE DATABASE StudentsData;
```

``` SQL
  CREATE TABLE Students (
    StudentID int,
    FirstName varchar(255),
    LastName varchar(255),
    RollNo. int,
    Address varchar(255),
    City varchar(255)
  );
```

***

### DROP

This command is used to drop an existing database.

```SQL

  DROP DATABASE StudentsData;
```

***

### ALTER

This command is used to delete column in the table.

```SQL
  ALTER TABLE Students
  DROP COLUMN City;
```

***

## DML (Data Manipulation Language)

***
Data Manipulation Language(DML) is used to manipulate or update the table in the database. Common example of DML statements are `SELECT`, `INSERT`, `UPDATE`, `DELETE`, etc.

## Some Examples

### SELECT

This command is used to retrieve data from the database.

```SQL
  SELECT * FROM Students;
```

***

### INSERT

This command is used to insert data into a table.

```SQL
  INSERT INTO Students
  VALUES (1, 'Prateek', 'Madhesia', 103, 'Karkend Bazar', 'Dhanbad');
```

***

### DELETE

This command is used to deletes all records from a table.

```SQL
  DELETE FROM Students WHERE RollNo=100;
```

***

### UPDATE

This command is used to update existing data within a table.

```SQL
  UPDATE Students
  SET StudentID = 7
  WHERE RollNo=103;
```

***

## TCL (Transaction Control Language)

***

Transaction Control Language(TCL) is used to manage transactions in the database. Common example of TCL statements are `COMMIT`, `ROLLBACK`, `SAVEPOINT`, etc.

## Some Example

### COMMIT

This command is used to permanently save any transaction into the database.

```SQL
  COMMIT;
```

***

### ROLLBACK

This command is used to restores the database to the last committed state.

```SQL
  ROLLBACK TO savepoint_name;
```

***

### SAVEPOINT

This command is used to temporarily save a transaction so that you can rollback to that point whenever required.

```SQL
  SAVEPOINT savepoint_name;
```

***

## DCL (Data Control Language)

***

Data Control Language(DCL) is used to control access to data stored in a database (Authorization). Common example of DCL statements are `GRANT` and `REVOKE`.

## Some Example

### GRANT

This command is used to provide any user access privileges or other privileges for the database.

```SQL
  GRANT CREATE TABLE TO Students;
```

***

### REVOKE

This command is used to  take back permissions from any user.

```SQL
  ROLLBACK TO REVOKE CREATE TABLE FROM Students;
```

***

## Joins

SQL `JOIN` Command is used to combine data or rows of two or more tables, based on common column between them.

### Different Types of SQL Joins

1. **INNER JOIN**
2. **LEFT JOIN**
3. **RIGHT JOIN**
4. **FULL JOIN**

![Types of join](https://cdn.educba.com/academy/wp-content/uploads/2019/10/Types-of-Joins-in-SQl.png.webp)

## INNER JOIN

`INNER JOIN` select all the rows from both the tables as long as the condition satisfies.

```SQL
SELECT Students.RollNO, Teachers.Name FROM Students INNER JOIN Teachers ON Students.ClassTeacher = Teachers.ClassTeacher;
```

***

## LEFT JOIN

`LEFT JOIN` select all the records from the first table and the matching record of second table as long as the condition satisfies.

```SQL
SELECT Students.*, Teachers.Name FROM Students LEFT JOIN Teachers ON Students.ClassTeacher = Teachers.ClassTeacher;
```

***

## RIGHT JOIN

`RIGHT JOIN` select all the records from the second table and the matching record of first table as long as the condition satisfies.

```SQL
SELECT Students.RollNO, Teachers.* FROM Students RIGHT JOIN Teachers ON Students.ClassTeacher = Teachers.ClassTeacher;
```

***

## FULL JOIN

`FULL JOIN` select all the records from the first table and the all record of second table as long as the condition satisfies.

```SQL
SELECT Students.*, Teachers.* FROM Students LEFT JOIN Teachers ON Students.ClassTeacher = Teachers.ClassTeacher;
```

***

## Aggregate Function

SQL aggregate function is used to perform calculation on multiple rows of single column of a table.

### Various SQL Aggregation Function

1. **COUNT()**
2. **Sum()**
3. **Avg()**
4. **Min()**
5. **Max()**

![SQL Aggregate](https://static.javatpoint.com/dbms/images/dbms-sql-aggregate-functio.png)

### COUNT()

This function is used to count number of rows of a table.

```SQL
SELECT COUNT(*)  
FROM Students;  
```

***

### SUM()

This function is used to calculate sum of all non null values of selected column in a table.

```SQL
SELECT SUM(Marks)  
FROM Students;  
```

***

### AVG()

This function is used to calculate average of all non null values of selected column in a table.

```SQL
SELECT AVG(TotalMarks)  
FROM Students;  
```

***

### MIN()

This function is used to calculate minimum of all non null values of selected column in a table.

```SQL
SELECT MIN(Marks)  
FROM Students;  
```

***

### MAX()

This function is used to calculate maximum of all non null values of selected column in a table.

```SQL
SELECT MAX(Marks)  
FROM Students;  
```

***

## References

- **_[Geeks of Geeks](https://www.geeksforgeeks.org/sql-tutorial/)_**

- **_[Java T Point](https://www.javatpoint.com/sql-tutorial)_**

- **_[W3 School](https://www.w3schools.com/sql/)_**
